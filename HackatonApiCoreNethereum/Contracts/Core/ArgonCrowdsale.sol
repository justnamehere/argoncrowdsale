pragma solidity ^0.4.24;

import "../Openzeppelin/Tokens/ERC20/ERC20Mintable.sol";
import "../Openzeppelin/Tokens/ERC20/ERC20Burnable.sol";
import "./Argon.sol";
import "../Openzeppelin/Crowdsale/Emission/AllowanceCrowdsale.sol";
import "../Openzeppelin/Crowdsale/Validation/TimedCrowdsale.sol";

contract ArgonCrowdsale is AllowanceCrowdsale, TimedCrowdsale{
    constructor(
        uint256 _rate,
        address _wallet,
        IERC20 _token,
        uint256 _openingTime,
        uint256 _closingTime
        )payable public
        Crowdsale(_rate, _wallet, _token)
        AllowanceCrowdsale(_wallet)
        TimedCrowdsale(_openingTime, _closingTime){
        
    }
}