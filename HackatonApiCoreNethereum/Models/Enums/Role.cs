using System.ComponentModel;

namespace HackatonApiCoreNethereum.Models.Enums{
    public enum Role
    {
        [Description("Admin")]
        Admin = 1,

        [Description("User")]
        User = 2,

        [Description("Moderator")]
        Moderator = 3,
    }
}