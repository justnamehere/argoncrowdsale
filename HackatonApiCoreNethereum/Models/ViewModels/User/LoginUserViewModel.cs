using System.ComponentModel.DataAnnotations;

namespace HackatonApiCoreNethereum.Models.ViewModels.User
{
    public class LoginUserViewModel{

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}