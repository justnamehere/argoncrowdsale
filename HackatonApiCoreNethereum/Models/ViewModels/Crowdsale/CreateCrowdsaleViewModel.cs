using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace HackatonApiCoreNethereum.Models.ViewModels.Crowdsale{
    public class CreateCrowdsaleViewModel{        
        public int Rate { get; set; }
        //public string Wallet { get; set; }
        //public string TockenAddress { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime CloseTime { get; set; }
    }
}