using System.Threading.Tasks;
using HackatonApiCoreNethereum.Attributes;
using HackatonApiCoreNethereum.Models.Enums;
using HackatonApiCoreNethereum.Models.ViewModels.Crowdsale;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace HackatonApiCoreNethereum{
    public class DeploymentController:Controller{
        private readonly IDeploymentService service;

        public DeploymentController(IDeploymentService service){
            this.service = service;
        }

        [HttpPost("Deploy/DeployArgonCrowdsaleContract")]
        public async Task<IActionResult> DeployArgonCrowdsaleContract([FromForm]CreateCrowdsaleViewModel model){
            var result = await service.DeployArgonCrowdsaleContract(model);
            if(result.Succeeded)
                return Json(result.Message);           
            
            return BadRequest(result.Message);
        } 
        
        [HttpPost("Deploy/DeployArgonTokenContract")]
        public async Task<IActionResult> DeployArgonTokenContract(){
            var result = await service.DeployArgonTokenContract();
            if(result.Succeeded)
                return Json(result.Message);           
            
            return BadRequest(result.Message);
        }

        [EnumRole(Role.User, AuthenticationSchemes = "Bearer")]
        [HttpPost("Argon/MintForTest")]
        public async Task<IActionResult> MintForTest(){
            var result = await service.MintForTest(User.Identity.Name);
            if(result.Succeeded)
                return Json(result.Message);           
            
            return BadRequest(result.Message);
        }
    }
}