using System;
using System.Linq;
using HackatonApiCoreNethereum.Models.Enums;
using Microsoft.AspNetCore.Authorization;

namespace HackatonApiCoreNethereum.Attributes{
    public class EnumRoleAttribute : AuthorizeAttribute{
        public EnumRoleAttribute(params Role[] roles)
        {
            this.Roles = string.Join(",", roles.Select(r => Enum.GetName(r.GetType(), r)));
        }
    }
}