using System;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;

namespace HackatonApiCoreNethereum.Services.Interfaces{
    
    public class TokenService:ITokenService
    {
        private readonly Web3 web;
        private readonly string argonTokenAbi;
        private readonly string argonTokenAddress;
        private readonly string managerAddress;
        private readonly UserManager<ApplicationUser> userManager;

        public TokenService(IConfiguration config, UserManager<ApplicationUser>userManager){
            this.userManager = userManager;
            this.web = new Web3(config["BlockchainSetting:Web3Url"]);

            this.argonTokenAddress = config["BlockchainSetting:ArgonTokenAddress"];
            this.argonTokenAbi = config["BlockchainSetting:ArgonTokenAbi"];
            this.managerAddress = config["BlockchainSetting:ManagerAddress"];            
        }

        public async Task<CustomIdentityResult> AddMinter(string address)
        {
            try
            {                
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var addMinterFunc = argonContract.GetFunction("addMinter");

                /*var estimateGasApproveFunc = await approveFunc.EstimateGasAsync(user.PublicAddress, null, null,
                 currentEvent.EthAddress, ticketPrice); */
                var transactionAddMinter = await addMinterFunc.SendTransactionAsync(managerAddress,
                new HexBigInteger(3000000), null,address);

                return new CustomIdentityResult(true, transactionAddMinter);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);
            }
        }

        public async Task<CustomIdentityResult> Allowance(string owner, string spender)
        {
          try
            {
                var user = await userManager.FindByNameAsync(owner);         
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var allowanceFunc = argonContract.GetFunction("allowance");

                /*var estimateGasApproveFunc = await approveFunc.EstimateGasAsync(user.PublicAddress, null, null,
                 currentEvent.EthAddress, ticketPrice); */
                var AllowanceValue = await allowanceFunc.CallAsync<int>(user.PublicAddress,
                null, null, user.PublicAddress, spender);

                return new CustomIdentityResult(true, AllowanceValue);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);
            }
        }

        public async Task<CustomIdentityResult> Approve(string username, string address, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var approveFunc = argonContract.GetFunction("approve");
                
                var transactionApprove = await approveFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, address, value );

                return new CustomIdentityResult(true, transactionApprove);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> BalanceOf(string owner)
        {
            try
            {
                var user = await userManager.FindByNameAsync(owner);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var balanceOfFunc = argonContract.GetFunction("balanceOf");
                
                var balance = await balanceOfFunc.CallAsync<int>(user.PublicAddress,
                null, null, user.PublicAddress);

                return new CustomIdentityResult(true, balance);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Burn(string username, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var burnFunc = argonContract.GetFunction("burn");
                
                var transactionBurn = await burnFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, value );

                return new CustomIdentityResult(true, transactionBurn);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> BurnFrom(string username, string address, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var burnFromFunc = argonContract.GetFunction("burnFrom");
                
                var transactionBurnFrom = await burnFromFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, address, value );

                return new CustomIdentityResult(true, transactionBurnFrom);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Decimals()
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var decimalsFunc = argonContract.GetFunction("decimals");
                
                var Decimals = await decimalsFunc.CallAsync<int>(managerAddress, null, null);

                return new CustomIdentityResult(true, Decimals);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> DecreaseAllowance(string username, string spender, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var decreaseAllowanceFunc = argonContract.GetFunction("decreaseAllowance");
                
                var transactionDecreaseAllowanceFunc = await decreaseAllowanceFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, spender, value );

                return new CustomIdentityResult(true, transactionDecreaseAllowanceFunc);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public async Task<CustomIdentityResult> IncreaseAllowance(string username, string spender, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var increaseAllowanceFunc = argonContract.GetFunction("increaseAllowance");
                
                var transactionIncreaseAllowanceFunc = await increaseAllowanceFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, spender, value );

                return new CustomIdentityResult(true, transactionIncreaseAllowanceFunc);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> isMinter(string address)
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var isMinterFunc = argonContract.GetFunction("isMinter");
                
                var result = await isMinterFunc.CallAsync<bool>(managerAddress, null, null, address);

                return new CustomIdentityResult(true, result);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Mint(string address, int value)
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var mintFunc = argonContract.GetFunction("mint");
                
                var transaction = await mintFunc.SendTransactionAsync(managerAddress, new HexBigInteger(3000000),
                 null, address, value);

                return new CustomIdentityResult(true, transaction);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Name()
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var nameFunc = argonContract.GetFunction("name");
                
                var name = await nameFunc.CallAsync<string>(managerAddress, null, null);

                return new CustomIdentityResult(true, name);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> RenounceMinter()
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var renounceMinterFunc = argonContract.GetFunction("renounceMinter");
                
                var transaction = await renounceMinterFunc.SendTransactionAsync(managerAddress, new HexBigInteger(3000000),
                 null);

                return new CustomIdentityResult(true, transaction);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Symbol()
        {
            try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var symbolFunc = argonContract.GetFunction("symbol");
                
                var symbol = await symbolFunc.CallAsync<string>(managerAddress, null, null);

                return new CustomIdentityResult(true, symbol);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> TotalSupply()
        {
           try
            {
                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var totalSupplyFunc = argonContract.GetFunction("totalSupply");
                
                var supply = await totalSupplyFunc.CallAsync<string>(managerAddress, null, null);

                return new CustomIdentityResult(true, supply);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> Transfer(string username, string to, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var transferFunc = argonContract.GetFunction("transfer");
                
                var transactionDecreaseAllowanceFunc = await transferFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, to, value );

                return new CustomIdentityResult(true, transactionDecreaseAllowanceFunc);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }

        public async Task<CustomIdentityResult> TransferFrom(string username, string from, string to, int value)
        {
            try
            {
                var user = await userManager.FindByNameAsync(username);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var transferFromFunc = argonContract.GetFunction("transferFrom");
                
                var transaction = await transferFromFunc.SendTransactionAsync(user.PublicAddress,
                new HexBigInteger(3000000), null, from, user.PublicAddress, value );

                return new CustomIdentityResult(true, transaction);
            }
            catch (System.Exception exe)
            {                
                return new CustomIdentityResult(false, exe.Message);;
            }
        }
    }
}