using System;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.Crowdsale;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;

namespace HackatonApiCoreNethereum.Services
{
    public class DeploymentService : IDeploymentService
    {
        private readonly Web3 Web;

        private readonly string ArgonCrowdsaleAbi;
        private readonly string ArgonCrowdsaleByteCode;

        private readonly string ArgonTokenAbi;
        private readonly string ArgonTokenByteCode;
        private readonly string ArgonTokenAddress;
        private readonly string ManagerAddress;

        private readonly UserManager<ApplicationUser> Manager;

        public DeploymentService(IConfiguration config, UserManager<ApplicationUser> Manager)
        {
            this.Web = new Web3(config["BlockchainSetting:Web3Url"]);
            this.ManagerAddress = config["BlockchainSetting:ManagerAddress"];
            this.ArgonTokenAbi = config["BlockchainSetting:ArgonTokenAbi"];
            this.ArgonTokenByteCode = config["BlockchainSetting:ArgonTokenByteCode"];
            this.ArgonTokenAddress = config["BlockchainSetting:ArgonTokenAddress"];
            this.ArgonCrowdsaleAbi = config["BlockchainSetting:ArgonCrowdsaleAbi"];
            this.ArgonCrowdsaleByteCode = config["BlockchainSetting:ArgonCrowdsaleByteCode"];
            this.Manager = Manager; 
        }
        public async Task<CustomIdentityResult> DeployArgonTokenContract()
        {
            try
            {
                var transaction = await Web.Eth.DeployContract.SendRequestAndWaitForReceiptAsync(ArgonTokenAbi,
                ArgonTokenByteCode, ManagerAddress,
                new HexBigInteger(3000000), null);

                return new CustomIdentityResult(true, transaction.ContractAddress);
            }
            catch (System.Exception exeption)
            {
                return new CustomIdentityResult(false, exeption.Message);
            }
        }

        public async Task<CustomIdentityResult> MintForTest(string username)
        {
            try
            {
                var user = await Manager.FindByNameAsync(username);
                var argonContract = Web.Eth.GetContract(ArgonTokenAbi, ArgonTokenAddress);

                var mintFunc = argonContract.GetFunction("mint");
                var estimatedGasMintFunc = await mintFunc.EstimateGasAsync(ManagerAddress,null,null,
                 user.PublicAddress, 1000);

                var transaction = await mintFunc.SendTransactionAsync(ManagerAddress,estimatedGasMintFunc,null,
                 user.PublicAddress, 1000);

                return new CustomIdentityResult(true, transaction);
            }
            catch (Exception exeption)
            {
                return new CustomIdentityResult(false, exeption.Message);
            }
        }
    
        public async Task<CustomIdentityResult> DeployArgonCrowdsaleContract(CreateCrowdsaleViewModel model){
            try
            {
                var transaction = await Web.Eth.DeployContract.SendRequestAndWaitForReceiptAsync(ArgonCrowdsaleAbi,
                ArgonCrowdsaleByteCode, ManagerAddress,
                new HexBigInteger(3000000), null,
                 model.Rate, ManagerAddress, ArgonTokenAddress, model.OpenTime.Ticks,
                  model.CloseTime.Ticks);

                return new CustomIdentityResult(true, transaction.ContractAddress);
            }
            catch (Exception exeption)
            {
                return new CustomIdentityResult(false, exeption.Message);
            }
        }
    }
}