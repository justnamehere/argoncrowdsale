using System.Collections.Generic;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.Crowdsale;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Services.Interfaces{
    public interface IDeploymentService
    {
        Task<CustomIdentityResult> DeployArgonCrowdsaleContract(CreateCrowdsaleViewModel model);
        Task<CustomIdentityResult> DeployArgonTokenContract();
        Task<CustomIdentityResult> MintForTest(string username);
    }
}