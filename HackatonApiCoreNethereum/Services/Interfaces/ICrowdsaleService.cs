using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Services.Interfaces{
    public interface ICrowdsaleService:IDisposable
    {        
        Task<CustomIdentityResult> BuyTokens(string address);
        Task<CustomIdentityResult> ClosingTime();
        Task<CustomIdentityResult> HasClosed();
        Task<CustomIdentityResult> IsOpen();
        Task<CustomIdentityResult> OpeningTime();
        Task<CustomIdentityResult> Rate();
        Task<CustomIdentityResult> RemainingTokens();
        Task<CustomIdentityResult> Token();
        Task<CustomIdentityResult> TokenWallet();
        Task<CustomIdentityResult> Wallet();
        Task<CustomIdentityResult> WeiRaised();
    }
}