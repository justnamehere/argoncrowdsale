using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Services.Interfaces{
    public interface ITokenService:IDisposable
    {
        
        Task<CustomIdentityResult> AddMinter(string address);
        Task<CustomIdentityResult> Approve(string username,string address, int value);
        Task<CustomIdentityResult> Burn(string username,int value);
        Task<CustomIdentityResult> BurnFrom(string username,string address, int value);
        Task<CustomIdentityResult> DecreaseAllowance(string username,string spender, int value);
        Task<CustomIdentityResult> IncreaseAllowance(string username,string spender, int value);
        Task<CustomIdentityResult> Mint(string address, int value);
        Task<CustomIdentityResult> RenounceMinter();
        Task<CustomIdentityResult> Transfer(string username,string to, int value);
        Task<CustomIdentityResult> TransferFrom(string username,string from, string to, int value);
        Task<CustomIdentityResult> Allowance(string username, string spender);
        Task<CustomIdentityResult> BalanceOf(string username);
        Task<CustomIdentityResult> Decimals();
        Task<CustomIdentityResult> isMinter(string address);
        Task<CustomIdentityResult> Name();
        Task<CustomIdentityResult> Symbol();
        Task<CustomIdentityResult> TotalSupply();
       
    }
}