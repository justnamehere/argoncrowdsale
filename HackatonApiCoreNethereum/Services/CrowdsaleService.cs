using System;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity.User;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Services.Interfaces{

    public class CrowdsaleService : ICrowdsaleService
    {
        public Task<CustomIdentityResult> BuyTokens(string address)
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> ClosingTime()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> HasClosed()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> IsOpen()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> OpeningTime()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> Rate()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> RemainingTokens()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> Token()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> TokenWallet()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> Wallet()
        {
            throw new NotImplementedException();
        }

        public Task<CustomIdentityResult> WeiRaised()
        {
            throw new NotImplementedException();
        }
    }
}